import { useEffect, useState } from 'react';
import './App.css';

const quote = ['Hello World','I am Handsome!','Keep going!','good luck my friend','byebye your tile']

function App() {
  const [displayQuote, setDisplayQuote] = useState<string>(quote[0])
  const [count, setCount] = useState<number>(0)

  const updateQuote = () => {
    setCount(count + 1)
  }

  useEffect(() => {
    setDisplayQuote(quote[count % 5])
  },[count])

  return (
    <div className="App">
      <div className='quote'>This is a quote: {displayQuote}</div>
      <button onClick={() => updateQuote()}>new quote</button>
    </div>
  );
}

export default App;
